# FractalGen
A simple fractal image generator written in C++.

## Capabilities

- Generate Mandelbrot / Julia set BMP images
- Configure with a JSON file
- Chose a coloring method
- Apply post-process filters

## Get started

### Building the source code

1. Clone or download the repository
2. Configure with CMake
3. Build with CMake

### Downloading the executable

_Coming soon_

## JSON settings

You can specify the path to the JSON settings file as the first argument after the executable.

```ps1
./fractalgen.exe settings.json
```

First, you might want to specify the JSON schema in your JSON file in order to get a basic auto-completion.

```json
{
	"$schema": "https://dl.nicolasjullien.fr/configuration.fractalgen.schema.json"
}
```

### Image properties

You can configure the output image with the `image` object.

```json
{
	[...]
	"image": {
		"width": 3840,
		"height": 2160,
		"format": "bmp",
		"filename": "output.bmp"
	},
	[...]
}
```

The `width` and  `height` keys set the image canvas size, the `format` key sets the file format of the image (for now, only BMP is supported) and the `filename` key sets the path where the file will be written.

### Generator properties

The `generator` object defines the settings that will be used by the fractal generator.

```json
{
	[...]
	"generator": {
		"name": "Julia",
		"center": {
			"i": 0,
			"r": 0
		},
		"zoom": {
			"x": 1000,
			"y": 1000
		},
		"max_iterations": 200,
		"params": {
			"c": {
				"i": 0.6,
				"r": -0.4
			}
		}
	},
	[...]
}
```

The `name` key allows you to chose between the two currently available generators : "Julia" and "Mandelbrot".

The `center` key sets the complex point that the image will be centered on.

The `zoom` key sets the zoom that will be used in the image. A (1;1) zoom corresponds to 1 horizontal pixel = 1 real unit ; 1 vertical pixel = 1 complex unit (i). The size of the image do not affect the zoom.

The `max_iterations` key sets the limit of iterations to perform before considering that the complex point is in the Julia / Mandelbrot set.

The `params` key is an object where you can define parameters for the generators. The Julia generator requires you to define the parameter `c`, which is the value to add at each iteration.

### Colorizer properties

The `colorizer` object defines the settings that will be used by the coloring algorithms.

```json
{
	[...]
	"colorizer": {
		"name": "TwoColors",
		"params": {
			"color_a": {
				"r": 1,
				"g": 0,
				"b": 0
			},
			"color_b": {
				"r": 0,
				"g": 0,
				"b": 0
			},
			"speed": 0.1
		}
	},
	[...]
}
```

The `name` key allows you to chose what coloring algorithm you want to use.

The `params` key is an object where you can define parameters for the colorizer. For example, the TwoColors generator requires you to define the color parameters `color_a`, `color_b` and `speed`.

### Postprocess properties

The `postprocess` object defines the effects that will be applied to the generated image.

```json
{
	[...]
	"postprocess": {
		"enabled": true,
		"effects": [
			[...]
		]
	}
	[...]
}
```

The `enabled` key allows you to quickly enable or disable all of the effects without needing to empty the effects list.

The `effect` key is an array which can be filled with one or more effects.