#pragma once
#include <memory>
#include "Configuration.hpp"
#include "Image.hpp"
#include "Color/Color.hpp"
#include "Color/Colorizer.hpp"

namespace Fractal
{
	class Generator
	{
		const Configuration* m_configuration;

	public:
		/**
		 * Instantiates a specialized generator according to the given configuration.
		 */
		static std::unique_ptr<Generator> get(const Configuration* _configuration);

		Generator(const Configuration* _configuration);

		/**
		 * Gets the configuration used in this generator.
		 */
		const Configuration* configuration() const;

		/**
		 * Computes the color at a pixel, with the given colorizer.
		 */
		virtual Color::Color render(double _x, double _y, const Color::Colorizer* _colorizer) const = 0;
	};

	inline Generator::Generator(const Configuration* _configuration)
		: m_configuration(_configuration)
	{}

	inline const Configuration* Generator::configuration() const
	{
		return m_configuration;
	}
	
} // namespace Fractal
