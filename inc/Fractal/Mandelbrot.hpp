#pragma once
#include "Generator.hpp"

namespace Fractal
{
	class Mandelbrot : public Generator
	{
	public:
		using Generator::Generator;
		Color::Color render(double _x, double _y, const Color::Colorizer* _colorizer) const override;
	};

} // namespace Fractal

