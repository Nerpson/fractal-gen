#pragma once
#include <memory>
#include "Configuration.hpp"
#include "Complex.hpp"
#include "Color.hpp"

namespace Color
{
	class Colorizer
	{
		const Configuration* m_configuration;
		
	public:
		/**
		 * Instantiates a specialized colorizer according to the given configuration.
		 */
		static std::unique_ptr<Colorizer> get(const Configuration* _configuration);
	
		Colorizer(const Configuration* _configuration);

		/**
		 * Gets the configuration used by this colorizer.
		 */
		const Configuration* configuration() const;

		/**
		 * Gets the color at the given complex position, with the given number of iterations.
		 */
		virtual Color get_color(const Complex& _position, unsigned _iterations) const = 0;
	};

	inline Colorizer::Colorizer(const Configuration* _configuration)
		: m_configuration(_configuration)
	{}

	inline const Configuration* Colorizer::configuration() const
	{
		return m_configuration;
	}
	
} // namespace Color
