#pragma once
#include "Colorizer.hpp"
#include "Complex.hpp"

namespace Color
{
	class Spectrum : public Colorizer
	{
	public:
		using Colorizer::Colorizer;
		Color get_color(const Complex& _complex, unsigned _iterations) const override;
	};
	
} // namespace Color
