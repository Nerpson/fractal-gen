#pragma once
#include "Colorizer.hpp"

namespace Color
{
	class BlackAndWhite : public Colorizer
	{
	public:
		using Colorizer::Colorizer;
		Color get_color(const Complex& _position, unsigned _iterations) const override;
	};
	
} // namespace Color
