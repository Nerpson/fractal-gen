#pragma once
#include <algorithm>

namespace Color
{
	class Color
	{
		double m_r = 0.;
		double m_g = 0.;
		double m_b = 0.;
		double m_a = 1.;

	public:
		/**
		 * Creates a RGB color from HSL components.
		 */
		static Color from_hsl(double _h, double _s, double _l);
	
		Color(double _r, double _g, double _b, double _a);
		Color(double _r, double _g, double _b);
		Color();

		/**
		 * Gets the red component of the color.
		 */
		double r() const;

		/**
		 * Gets the green component of the color.
		 */
		double g() const;

		/**
		 * Gets the blue component of the color.
		 */
		double b() const;

		/**
		 * Gets the alpha (transparency) component of the color.
		 */
		double a() const;

		Color& operator +=(const Color& _right);
		Color& operator /=(double _right);

		friend bool operator ==(const Color& _left, const Color& _right);
		friend Color operator +(const Color& _left, const Color& _right);
		friend Color operator -(const Color& _left, const Color& _right);

		template <typename T>
		friend Color operator *(const Color& _left, T _right);
		template <typename T>
		friend Color operator /(const Color& _left, T _right);
	};

	inline Color::Color(double _r, double _g, double _b)
		: Color(_r, _g, _b, 1.)
	{}

	inline Color::Color() {}

	inline double Color::r() const
	{
		return m_r;
	}

	inline double Color::g() const
	{
		return m_g;
	}

	inline double Color::b() const
	{
		return m_b;
	}

	inline double Color::a() const
	{
		return m_a;
	}

	template <typename T>
	Color operator *(const Color& _left, T _right)
	{
		return Color(_left.m_r * _right, _left.m_g * _right, _left.m_b * _right, std::clamp(_left.m_a * _right, 0., 1.));
	}

	template <typename T>
	Color operator /(const Color& _left, T _right)
	{
		return Color(_left.m_r / _right, _left.m_g / _right, _left.m_b / _right, std::clamp(_left.m_a / _right, 0., 1.));
	}

} // namespace Color
