#pragma once
#include "Colorizer.hpp"
#include "Color.hpp"

namespace Color
{
	
	class TwoColors : public Colorizer
	{
		Color m_color_a;
		Color m_color_b;
		double m_speed;
		
	public:
		TwoColors(const Configuration* _configuration);
		TwoColors(const Configuration* _configuration, const Color& _color_a, const Color& _color_b, double _speed);
		
		virtual Color get_color(const Complex& _position, unsigned _iterations) const;
	};
	
} // namespace Color
