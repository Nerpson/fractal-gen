#pragma once

class Tile
{
	unsigned m_from_x;
	unsigned m_from_y;
	unsigned m_to_x;
	unsigned m_to_y;
	
public:
	Tile(unsigned _from_x, unsigned _from_y, unsigned _to_x, unsigned _to_y);

	/**
	 * Gets the top-left pixel x coordinate of the tile.
	 */
	unsigned from_x() const;
	
	/**
	 * Gets the top-left pixel y coordinate of the tile.
	 */
	unsigned from_y() const;
	
	/**
	 * Gets the bottom-right pixel x coordinate of the tile.
	 */
	unsigned to_x() const;
	
	/**
	 * Gets the bottom-right pixel y coordinate of the tile.
	 */
	unsigned to_y() const;
};

inline unsigned Tile::from_x() const
{
	return m_from_x;
}

inline unsigned Tile::from_y() const
{
	return m_from_y;
}

inline unsigned Tile::to_x() const
{
	return m_to_x;
}

inline unsigned Tile::to_y() const
{
	return m_to_y;
}
