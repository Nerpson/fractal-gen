#pragma once
#include "TileQueue.hpp"
#include "ImageSharer.hpp"
#include "Fractal/Generator.hpp"
#include "Color/Colorizer.hpp"

class RenderThread
{
	TileQueue* m_tile_queue;
	ImageSharer& m_image_sharer;
	const Fractal::Generator* m_generator;
	const Color::Colorizer* m_colorizer;

public:
	RenderThread(TileQueue* _tile_queue, ImageSharer& m_image_sharer, const Fractal::Generator* _generator, const Color::Colorizer* _colorizer);

	/**
	 * Runs the code, normally in a thread.
	 */
	void operator ()() const;
};
