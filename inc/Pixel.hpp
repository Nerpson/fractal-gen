#pragma once
#include "Color/Color.hpp"

struct Pixel
{
	double r = 0.;
	double g = 0.;
	double b = 0.;

	/**
	 * Converts this struct's data to a color objet.
	 */
	Color::Color color() const;

	/**
	 * Sets this struct's data from a color object.
	 */
	void set_color(const Color::Color& _color);
};

inline Color::Color Pixel::color() const
{
	return Color::Color(r, g, b);
}
