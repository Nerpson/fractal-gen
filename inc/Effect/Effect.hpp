#pragma once
#include "Configuration.hpp"
#include "Image.hpp"
#include <memory>

namespace Effect
{
	class Effect
	{
		const Configuration* m_configuration;
		unsigned m_id;
		
	public:
		/**
		 * Instantiates a vector of specialized effects according to the given configuration.
		 */
		static std::unique_ptr<std::vector<std::unique_ptr<Effect>>> get(const Configuration* _configuration);
	
		Effect(const Configuration* _configuration, unsigned _id);

		/**
		 * Gets the configuration used in this effect.
		 */
		const Configuration* configuration() const;

		/**
		 * Gets the position of this effect in the effects queue.
		 */
		unsigned id() const;

		/**
		 * Creates a copy of the image on which the effect has been applied.
		 */
		virtual std::unique_ptr<Image> render(Image* _image) const = 0;
	};

	inline const Configuration* Effect::configuration() const
	{
		return m_configuration;
	}

	inline unsigned Effect::id() const
	{
		return m_id;
	}
	
} // namespace Effect
