#pragma once
#include "Effect.hpp"

namespace Effect
{
	class Bloom : public Effect
	{
	public:
		using Effect::Effect;
		std::unique_ptr<Image> render(Image* _image) const override;
	};
	
} // namespace Effect
