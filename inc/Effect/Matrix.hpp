#pragma once
#include "Effect.hpp"

namespace Effect
{
	class Matrix : public Effect
	{
	public:
		using Effect::Effect;
		std::unique_ptr<Image> render(Image* _image) const override;
	};

} // namespace Effect
