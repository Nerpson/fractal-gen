#pragma once
#include <string>
#include <memory>
#include "Complex.hpp"
#include "Parameters.hpp"

struct Configuration
{
	struct Effect
	{
		std::string name;
		Parameters params;
	};
	
	struct {
		unsigned width;
		unsigned height;
		std::string format = "bmp";
		std::string filename = "output.bmp";
	} image;

	struct {
		std::string name;
		unsigned tile_size = 128;
		unsigned threads = 4;
		struct {
			double r;
			double i;
		} center;
		struct {
			double x;
			double y;
		} zoom;
		unsigned max_iterations;
		Parameters parameters;
	} generator;

	struct {
		std::string name;
		Parameters parameters;
	} colorizer;

	struct {
		bool enabled;
		std::vector<Effect> effects;
	} postprocess;

	/**
	 * Creates a unique pointer to a configuration object which has been initialized with a JSON config file.
	 */
	static std::unique_ptr<Configuration> from_file(const std::string& _filename);
};