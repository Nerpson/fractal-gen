#pragma once
#include <string>
#include <memory>
#include "Configuration.hpp"
#include "Fractal/Generator.hpp"
#include "Color/Colorizer.hpp"

class FractalGen
{
	std::string m_configuration_file_path;
	std::unique_ptr<Configuration> m_configuration;
	
	std::unique_ptr<Color::Colorizer> m_colorizer;
	std::unique_ptr<Fractal::Generator> m_generator;

	std::unique_ptr<Image> m_image;
	
public:
	/**
	 * Instantiates the FractalGen application.
	 * The _argv parameter must contain a string containing the path to the JSON file.
	 */
	FractalGen(int _argc, const char** _argv);

	/**
	 * Initializes the configuration, the generator, the colorizer and the image.
	 */
	void init();

	/**
	 * Runs the generation.
	 */
	void run();
};
