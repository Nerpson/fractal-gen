#pragma once
#include <map>

class Matrix
{
	unsigned m_size_x;
	unsigned m_size_y;
	std::map<std::pair<unsigned, unsigned>, double> m_matrix;

public:
	Matrix(unsigned _size_x, unsigned _size_y);

	/**
	 * Gets the width of the matrix.
	 */
	unsigned size_x() const;

	/**
	 * Gets the height of the matrix.
	 */
	unsigned size_y() const;

	/**
	 * Sets the value at the given position in the matrix.
	 */
	void set(unsigned _x, unsigned _y, double value);

	/**
	 * Gets the value at the given position in the matrix.
	 */
	double get(unsigned _x, unsigned _y) const;
};

inline unsigned Matrix::size_x() const
{
	return m_size_x;
}

inline unsigned Matrix::size_y() const
{
	return m_size_y;
}
