#pragma once

struct Coord2D
{
	double x;
	double y;
	
	Coord2D(double _x, double _y);
	friend bool operator ==(const Coord2D& _left, const Coord2D& _right);
};
