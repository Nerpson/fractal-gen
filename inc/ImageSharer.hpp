#pragma once
#include <mutex>
#include "Image.hpp"
#include "Color/Color.hpp"

/**
 * Helper class to help share an image between multiple threads.
 */
class ImageSharer
{
	Image* m_image;
	std::mutex m_mutex;

public:
	ImageSharer(Image* _image);

	/**
	 * Safely put a color at the given position in the image.
	 */
	void put_color(unsigned _x, unsigned _y, const Color::Color& _color);
};

inline ImageSharer::ImageSharer(Image* _image)
	: m_image(_image)
{}
