#pragma once
#include <queue>
#include <mutex>
#include "Tile.hpp"

class TileQueue
{
	std::queue<Tile*> m_queue;
	std::mutex m_mutex;

public:
	/**
	 * Gets the number of tiles in the queue.
	 */
	size_t size() const;

	/**
	 * Adds a tile at the end of the queue.
	 */
	void queue(Tile* _tile);

	/**
	 * Returns the next tile and removes it from the queue.
	 */
	Tile* next();
};

inline size_t TileQueue::size() const
{
	return m_queue.size();
}
