#pragma once

#include <map>
#include "Pixel.hpp"

class Image
{
	unsigned m_width;
	unsigned m_height;
	
	std::map<std::pair<unsigned, unsigned>, Pixel*> m_registered_pixels;
	
public:
	Image(unsigned _width, unsigned _height);
	Image(const Image& _image);
	~Image();

	/**
	 * Gets the width of the image.
	 */
	unsigned width() const;

	/**
	 * Gets the height of the image.
	 */
	unsigned height() const;

	/**
	 * Checks whether a pixel exists at the given position.
	 */
	bool pixel_exists_at(unsigned _x, unsigned _y) const;

	/**
	 * Gets the pixel at the given position.
	 */
	const Pixel& get_pixel_at(unsigned _x, unsigned _y) const;

	/**
	 * Gets the pixel at the given position.
	 */
	Pixel& get_pixel_at(unsigned _x, unsigned _y);

	/**
	 * Voids the pixel at the given position.
	 */
	void remove_pixel_at(unsigned _x, unsigned _y);
};

inline unsigned Image::width() const
{
	return m_width;
}

inline unsigned Image::height() const
{
	return m_height;
}