#pragma once
#include <vector>
#include <string>

class Binary
{
	std::vector<char> m_binary;

public:
	/**
	 * Clears the whole data.
	 */
	void clear();

	/**
	 * Gets the size in bytes.
	 */
	unsigned size() const;

	/**
	 * Adds the given byte.
	 */
	void add_byte(char _byte);

	/**
	 * Adds the given byte a specified amount of times.
	 */
	void add_byte(char _byte, unsigned _times);

	/**
	 * Adds the given bytes.
	 */
	void add_bytes(const char* _bytes, unsigned _size);

	/**
	 * Overwrites an existing byte.
	 */
	void set_byte(unsigned _offset, char byte);

	/**
	 * Adds 4 bytes representing the given integer.
	 */
	void add_integer(int _integer);

	/**
	 * Overwrites 4 existing bytes with the given integer.
	 */
	void set_integer(unsigned _offset, int _integer);

	/**
	 * Adds 2 bytes representing the given integer.
	 */
	void add_short_integer(short _short_integer);

	/**
	 * Gets the vector containing all bytes.
	 */
	const std::vector<char>& get_vector() const;
};

inline void Binary::clear()
{
	m_binary.clear();
}

inline unsigned Binary::size() const
{
	return m_binary.size();
}

inline const std::vector<char>& Binary::get_vector() const
{
	return m_binary;
}

/**
 * Writes a binary into a file.
 */
void write_to_file(const std::string& _filename, const Binary& _binary);