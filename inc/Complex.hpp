#pragma once
#include <ostream>

class Complex
{
	double m_i;
	double m_r;

public:
	Complex(const Complex& _other);
	Complex(double _i, double _r);
	Complex();

	/**
	 * Gets the imaginary part of the complex number.
	 */
	double i() const;
	
	/**
	 * Gets the real part of the complex number.
	 */
	double r() const;

	/**
	 * Calculates the magnitude of this complex number.
	 */
	double magnitude() const;

	Complex& operator =(const Complex& _other);

	friend bool operator ==(const Complex& _left, const Complex& _right);

	friend Complex operator +(const Complex& _left, const Complex& _right);
	template<typename T>
	friend Complex operator +(const Complex& _left, T _right);

	friend Complex operator *(const Complex& _left, const Complex& _right);
	template<typename T>
	friend Complex operator *(const Complex& _left, T _right);
	
	friend std::ostream& operator <<(std::ostream& _stream, const Complex& _complex);
};

inline Complex::Complex(const Complex& _other)
	: m_i(_other.m_i), m_r(_other.m_r)
{}

inline Complex::Complex(double _i, double _r)
	: m_i(_i), m_r(_r)
{}

inline Complex::Complex()
	: Complex(0., 0.)
{}

inline double Complex::i() const
{
	return m_i;
}

inline double Complex::r() const
{
	return m_r;
}

inline double Complex::magnitude() const
{
	return sqrt(pow(m_i, 2) + pow(m_r, 2));
}

template<typename T>
Complex operator +(const Complex& _left, T _right)
{
	return Complex(_left.m_i, _left.m_r + _right);
}

template<typename T>
Complex operator *(const Complex& _left, T _right)
{
	return Complex(_left.m_i * _right, _left.m_r * _right);
}