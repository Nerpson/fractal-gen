#pragma once
#include <map>
#include <string>
#include <vector>
#include "Coord2D.hpp"
#include "Color/Color.hpp"
#include "Complex.hpp"
#include "Matrix.hpp"

class Parameters
{
	std::map<std::string, bool> m_booleans;
	std::map<std::string, long long> m_integers;
	std::map<std::string, double> m_numbers;
	std::map<std::string, std::string> m_strings;
	std::map<std::string, Coord2D> m_coords;
	std::map<std::string, Color::Color> m_colors;
	std::map<std::string, Complex> m_complexes;
	std::map<std::string, Matrix> m_matrices;

public:
	/**
	 * Registers a parameter with a type and name.
	 */
	template <typename T>
	void add(const std::string& _param_name, T _value);

	/**
	 * Checks whether a parameter is registered with the given type and name.
	 */
	template <typename T>
	bool exists(const std::string& _param_name) const;

	/**
	 * Gets the parameter corresponding to the given type and name, or returns the default value.
	 */
	template <typename T>
	T get(const std::string& _param_name, T _default_value) const;
};