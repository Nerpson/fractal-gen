#pragma once
#include "Image.hpp"
#include "Binary.hpp"

/**
 * Converts an image to a binary object.
 */
void img_to_bitmap(const Image* _img, Binary& _binary);