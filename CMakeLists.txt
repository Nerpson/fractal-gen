cmake_minimum_required(VERSION 3.0.0)
project(fractal-gen VERSION 0.1.0)
set(CMAKE_CXX_STANDARD 17)

# include(CTest)
enable_testing()

set(SRC_DIR src)
set(INC_DIR inc)

include_directories(${INC_DIR})

file(GLOB_RECURSE SRC_FILES ${SRC_DIR}/*.cpp)
file(GLOB_RECURSE INC_FILES ${INC_DIR}/*.hpp)

add_executable(fractal-gen ${SRC_FILES})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})

include(CPack)
