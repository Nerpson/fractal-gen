#include "Configuration.hpp"
#include <fstream>
#include <iostream>
#include "nlohmann/json.hpp"
#include "Coord2D.hpp"

std::unique_ptr<Configuration> Configuration::from_file(const std::string& _filename)
{
	std::unique_ptr<Configuration> config(new Configuration());
	std::ifstream file(_filename);

	if (file.is_open())
	{
		nlohmann::json json;
		file >> json;

		auto image_node = json.find("image");
		if (image_node != json.end())
		{
			auto width_node = image_node->find("width");
			if (width_node != image_node->end()) config->image.width = width_node->get<unsigned>();
			else throw std::exception("Width not specified.");
			
			auto height_node = image_node->find("height");
			if (height_node != image_node->end()) config->image.height = height_node->get<unsigned>();
			else throw std::exception("Height not specified.");
			
			auto format_node = image_node->find("format");
			if (format_node != image_node->end()) config->image.format = format_node->get<std::string>();
			
			auto filename_node = image_node->find("filename");
			if (filename_node != image_node->end()) config->image.filename = filename_node->get<std::string>();
		}

		auto generator_node = json.find("generator");
		if (generator_node != json.end())
		{
			auto effect_name_node = generator_node->find("name");
			if (effect_name_node != generator_node->end()) config->generator.name = effect_name_node->get<std::string>();
			else throw std::exception("Generator name not specified.");
			
			auto threads_node = generator_node->find("threads");
			if (threads_node != generator_node->end()) config->generator.threads = threads_node->get<unsigned>();
			
			auto tile_size_node = generator_node->find("tile_size");
			if (tile_size_node != generator_node->end()) config->generator.tile_size = tile_size_node->get<unsigned>();
			
			auto center_node = generator_node->find("center");
			if (center_node != generator_node->end())
			{
				auto real_node = center_node->find("r");
				if (real_node != center_node->end()) config->generator.center.r = real_node->get<double>();
				else throw std::exception("Center real coordinate not specified.");

				auto imaginary_node = center_node->find("i");
				if (imaginary_node != center_node->end()) config->generator.center.i = imaginary_node->get<double>();
				else throw std::exception("Center imaginary coordinate not specified.");
			}
			else
			{
				throw std::exception("Center not specified.");
			}
			
			auto zoom_node = generator_node->find("zoom");
			if (zoom_node != generator_node->end())
			{
				auto x_node = zoom_node->find("x");
				if (x_node != zoom_node->end()) config->generator.zoom.x = x_node->get<double>();
				else throw std::exception("Zoom X coordinate not specified.");

				auto y_node = zoom_node->find("y");
				if (y_node != zoom_node->end()) config->generator.zoom.y = y_node->get<double>();
				else throw std::exception("Zoom Y coordinate not specified.");
			}
			else
			{
				throw std::exception("Zoom not specified.");
			}

			auto max_iterations_node = generator_node->find("max_iterations");
			if (max_iterations_node != generator_node->end()) config->generator.max_iterations = max_iterations_node->get<unsigned>();
			else throw std::exception("Max iterations not specified.");

			auto params_node = generator_node->find("params");
			if (params_node != generator_node->end())
			{
				for (auto it_effects = params_node->begin(); it_effects != params_node->end(); ++it_effects)
				{
					const std::string key = it_effects.key();
					if (it_effects->is_boolean())
					{
						config->generator.parameters.add(key, it_effects->get<bool>());
					}
					else if (it_effects->is_number())
					{
						config->generator.parameters.add(key, it_effects->get<double>());
						config->generator.parameters.add(key, it_effects->get<long long>());
					}
					else if (it_effects->is_string())
					{
						config->generator.parameters.add(key, it_effects->get<std::string>());
					}
					else if (it_effects->is_object())
					{
						auto x_node = it_effects->find("x");
						auto y_node = it_effects->find("y");
						if (x_node != it_effects->end() && y_node != it_effects->end())
						{
							// Coordinates found.
							config->generator.parameters.add(
								key,
								Coord2D(x_node->get<double>(), y_node->get<double>())
							);
						}
						else
						{
							auto i_node = it_effects->find("i");
							auto r_node = it_effects->find("r");
							if (i_node != it_effects->end() && r_node != it_effects->end())
							{
								// Complex number found.
								config->generator.parameters.add(
									key,
									Complex(i_node->get<double>(), r_node->get<double>())
								);
							}
						}
					}
					else
					{
						const std::string msg = "Unexpected type '" + std::string(it_effects->type_name()) + "' for colorizer parameters.";
						throw std::exception(msg.c_str());
					}
				}
			}
		}

		auto colorizer_node = json.find("colorizer");
		if (colorizer_node != json.end())
		{
			auto effect_name_node = colorizer_node->find("name");
			if (effect_name_node != colorizer_node->end()) config->colorizer.name = effect_name_node->get<std::string>();
			else throw std::exception("Colorizer name not specified.");

			auto params_node = colorizer_node->find("params");
			if (params_node != colorizer_node->end()) {
				for (auto it_effects = params_node->begin(); it_effects != params_node->end(); ++it_effects)
				{
					const std::string key = it_effects.key();
					if (it_effects->is_boolean())
					{
						config->colorizer.parameters.add(key, it_effects->get<bool>());
					}
					else if (it_effects->is_number())
					{
						config->colorizer.parameters.add(key, it_effects->get<double>());
						config->colorizer.parameters.add(key, it_effects->get<long long>());
					}
					else if (it_effects->is_string())
					{
						config->colorizer.parameters.add(key, it_effects->get<std::string>());
					}
					else if (it_effects->is_object())
					{
						auto r_node = it_effects->find("r");
						auto g_node = it_effects->find("g");
						auto b_node = it_effects->find("b");

						if (r_node != it_effects->end() && g_node != it_effects->end() && b_node != it_effects->end())
						{
							// Color found.
							config->colorizer.parameters.add(key, Color::Color(r_node->get<double>(), g_node->get<double>(), b_node->get<double>()));
						}
					}
					else
					{
						const std::string msg = "Unexpected type '" + std::string(it_effects->type_name()) + "' for colorizer parameters.";
						throw std::exception(msg.c_str());
					}
				}
			}
		}

		auto postprocess_node = json.find("postprocess");
		if (postprocess_node != json.end())
		{
			auto enabled_node = postprocess_node->find("enabled");
			if (enabled_node != postprocess_node->end()) config->postprocess.enabled = enabled_node->get<bool>();

			if (config->postprocess.enabled)
			{
				auto effects_node = postprocess_node->find("effects");
				if (effects_node != postprocess_node->end())
				{
					for (auto it_effects = effects_node->begin(); it_effects != effects_node->end(); ++it_effects)
					{
						if (it_effects->is_object())
						{
							auto effect_name_node = it_effects->find("name");
							if (effect_name_node != it_effects->end())
							{
								Parameters parameters;
								auto effects_params_node = it_effects->find("params");
								if (effects_params_node != it_effects->end())
								{
									for (auto it_params = effects_params_node->begin(); it_params != effects_params_node->end(); ++it_params)
									{
										if (it_params->is_object())
										{
											auto size_node = it_params->find("size");
											if (size_node != it_params->end())
											{
												Matrix matrix(size_node->find("x")->get<unsigned>(), size_node->find("y")->get<unsigned>());
												auto values_node = it_params->find("values");
												if (values_node != it_params->end())
												{
													unsigned y = 0;
													for (auto line : *values_node)
													{
														unsigned x = 0;
														for (auto value: line)
														{
															matrix.set(x++, y, value);
														}
														++y;
													}
												}

												parameters.add(it_params.key(), matrix);
											}
										}
									}
								}
								config->postprocess.effects.push_back(Configuration::Effect {effect_name_node->get<std::string>(), parameters});
							}
						}
					}
				}
			}
		}
		else
		{
			throw std::exception("Colorizer not specified.");
		}
		

		file.close();
	}

	return config;
}