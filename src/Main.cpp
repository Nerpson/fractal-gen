#include <iostream>
#include "FractalGen.hpp"

int main(int _argc, const char** _argv)
{
	try
	{
		FractalGen app(_argc, _argv);
		app.init();
		app.run();
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
