#include "Parameters.hpp"

template<>
void Parameters::add<bool>(const std::string& _param_name, bool _value)
{
	m_booleans.insert(std::make_pair(_param_name, _value));
}

template<>
void Parameters::add<long long>(const std::string& _param_name, long long _value)
{
	m_integers.insert(std::make_pair(_param_name, _value));
}

template<>
void Parameters::add<double>(const std::string& _param_name, double _value)
{
	m_numbers.insert(std::make_pair(_param_name, _value));
}

template<>
void Parameters::add<std::string>(const std::string& _param_name, std::string _value)
{
	m_strings.insert(std::make_pair(_param_name, _value));
}

template<>
void Parameters::add<Coord2D>(const std::string& _param_name, Coord2D _value)
{
	m_coords.insert(std::make_pair(_param_name, _value));
}

template<>
void Parameters::add<Color::Color>(const std::string& _param_name, Color::Color _value)
{
	m_colors.insert(std::make_pair(_param_name, _value));
}

template<>
void Parameters::add<Complex>(const std::string& _param_name, Complex _value)
{
	m_complexes.insert(std::make_pair(_param_name, _value));
}

template<>
void Parameters::add<Matrix>(const std::string& _param_name, Matrix _value)
{
	m_matrices.insert(std::make_pair(_param_name, _value));
}

template<>
bool Parameters::exists<bool>(const std::string& _param_name) const
{
	return m_booleans.find(_param_name) != m_booleans.end();
}

template<>
bool Parameters::exists<long long>(const std::string& _param_name) const
{
	return m_integers.find(_param_name) != m_integers.end();
}

template<>
bool Parameters::exists<double>(const std::string& _param_name) const
{
	return m_numbers.find(_param_name) != m_numbers.end();
}

template<>
bool Parameters::exists<std::string>(const std::string& _param_name) const
{
	return m_strings.find(_param_name) != m_strings.end();
}

template<>
bool Parameters::exists<Coord2D>(const std::string& _param_name) const
{
	return m_coords.find(_param_name) != m_coords.end();
}

template<>
bool Parameters::exists<Color::Color>(const std::string& _param_name) const
{
	return m_colors.find(_param_name) != m_colors.end();
}

template<>
bool Parameters::exists<Complex>(const std::string& _param_name) const
{
	return m_complexes.find(_param_name) != m_complexes.end();
}

template<>
bool Parameters::exists<Matrix>(const std::string& _param_name) const
{
	return m_matrices.find(_param_name) != m_matrices.end();
}

template<>
bool Parameters::get<bool>(const std::string& _param_name, bool _default_value) const
{
	auto it = m_booleans.find(_param_name);
	if (it != m_booleans.end()) return it->second;
	return _default_value;
}

template<>
long long Parameters::get<long long>(const std::string& _param_name, long long _default_value) const
{
	auto it = m_integers.find(_param_name);
	if (it != m_integers.end()) return it->second;
	return _default_value;
}

template<>
double Parameters::get<double>(const std::string& _param_name, double _default_value) const
{
	auto it = m_numbers.find(_param_name);
	if (it != m_numbers.end()) return it->second;
	return _default_value;
}

template<>
std::string Parameters::get<std::string>(const std::string& _param_name, std::string _default_value) const
{
	auto it = m_strings.find(_param_name);
	if (it != m_strings.end()) return it->second;
	return _default_value;
}

template<>
Coord2D Parameters::get<Coord2D>(const std::string& _param_name, Coord2D _default_value) const
{
	auto it = m_coords.find(_param_name);
	if (it != m_coords.end()) return it->second;
	return _default_value;
}

template<>
Color::Color Parameters::get<Color::Color>(const std::string& _param_name, Color::Color _default_value) const
{
	auto it = m_colors.find(_param_name);
	if (it != m_colors.end()) return it->second;
	return _default_value;
}

template<>
Complex Parameters::get<Complex>(const std::string& _param_name, Complex _default_value) const
{
	auto it = m_complexes.find(_param_name);
	if (it != m_complexes.end()) return it->second;
	return _default_value;
}

template<>
Matrix Parameters::get<Matrix>(const std::string& _param_name, Matrix _default_value) const
{
	auto it = m_matrices.find(_param_name);
	if (it != m_matrices.end()) return it->second;
	return _default_value;
}