#include "Complex.hpp"

Complex& Complex::operator =(const Complex& _other)
{
	if (this != &_other)
	{
		m_i = _other.m_i;
		m_r = _other.m_r;
	}
	return *this;
}

bool operator ==(const Complex& _left, const Complex& _right)
{
	return _left.m_i == _right.m_i && _left.m_r == _right.m_r;
}

Complex operator +(const Complex& _left, const Complex& _right)
{
	return Complex(_left.m_i + _right.m_i, _left.m_r + _right.m_r);
}

Complex operator *(const Complex& _left, const Complex& _right)
{
	double i = _left.m_i * _right.m_r + _left.m_r * _right.m_i;
	double r = -(_left.m_i * _right.m_i) + _left.m_r * _right.m_r;	
	
	return Complex(i, r);
}

std::ostream& operator <<(std::ostream& _stream, const Complex& _complex)
{
	return _stream << _complex.m_i << "i + " << _complex.m_r;
}