#include "Coord2D.hpp"

Coord2D::Coord2D(double _x, double _y)
	: x(_x), y(_y)
{}

bool operator ==(const Coord2D& _left, const Coord2D& _right)
{
	return _left.x == _right.x && _left.y == _right.y;
}