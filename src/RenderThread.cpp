#include "RenderThread.hpp"
#include <iostream>
#include "Pixel.hpp"

RenderThread::RenderThread(TileQueue* _tile_queue, ImageSharer& _image_sharer, const Fractal::Generator* _generator, const Color::Colorizer* _colorizer)
	: m_tile_queue(_tile_queue), m_image_sharer(_image_sharer), m_generator(_generator), m_colorizer(_colorizer)
{}

void RenderThread::operator ()() const
{
	const Configuration* configuration = m_generator->configuration();
	const double image_middle_x = double(configuration->image.width) / 2.;
	const double image_middle_y = double(configuration->image.height) / 2.;
	
	while (true)
	{
		Tile* next_tile = m_tile_queue->next();
		if (!next_tile) break;

		for (unsigned x = next_tile->from_x(); x <= next_tile->to_x(); ++x)
		{
			for (unsigned y = next_tile->from_y(); y <= next_tile->to_y(); ++y)
			{
				// Computing the pixel's position in the complex plane.
				double pos_i = (y - image_middle_y) / configuration->generator.zoom.y + configuration->generator.center.i;
				double pos_r = (x - image_middle_x) / configuration->generator.zoom.x + configuration->generator.center.r;
				
				m_image_sharer.put_color(x, y, m_generator->render(pos_r, pos_i, m_colorizer));
			}
		}
	}
}