#include "Color/BlackAndWhite.hpp"

namespace Color
{
	Color BlackAndWhite::get_color(const Complex& _position, unsigned _iterations) const
	{
		const Color starting_color(0., 0., 0.);
		const Color ending_color(1., 1., 1.);

		return starting_color + (ending_color - starting_color) * (double(_iterations) / double(configuration()->generator.max_iterations));
	}

} // namespace Color
