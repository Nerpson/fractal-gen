#include "Color/Colorizer.hpp"
#include <exception>
#include "Color/Ocean.hpp"
#include "Color/Spectrum.hpp"
#include "Color/BlackAndWhite.hpp"
#include "Color/TwoColors.hpp"

namespace Color
{
	std::unique_ptr<Colorizer> Colorizer::get(const Configuration* _configuration)
	{
		if (_configuration->colorizer.name == "Ocean")					return std::unique_ptr<Colorizer>(new Ocean(_configuration));
		if (_configuration->colorizer.name == "Spectrum")				return std::unique_ptr<Colorizer>(new Spectrum(_configuration));
		if (_configuration->colorizer.name == "BlackAndWhite")	return std::unique_ptr<Colorizer>(new BlackAndWhite(_configuration));
		if (_configuration->colorizer.name == "TwoColors")			return std::unique_ptr<Colorizer>(new TwoColors(_configuration));

		const std::string msg = "Unknown colorizer '" + _configuration->colorizer.name + "'.";
		throw std::exception(msg.c_str());
	}

} // namespace Color
