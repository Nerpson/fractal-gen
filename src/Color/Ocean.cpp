#include "Color/Ocean.hpp"

namespace Color
{
	Color Ocean::get_color(const Complex&, unsigned _iterations) const
	{
		const Color starting_color(0., 0.5, 1.);
		const Color ending_color(0., 0., 0.);

		return starting_color + (ending_color - starting_color) * (double(_iterations) / double(configuration()->generator.max_iterations));
	}

} // namespace Color
