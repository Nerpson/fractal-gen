#include "Color/Spectrum.hpp"

namespace Color
{
	Color Spectrum::get_color(const Complex&, unsigned _iterations) const
	{
		const Configuration* config = configuration();
		double hue_shift = config->colorizer.parameters.get("hue_shift", 0.);
		double hue_speed_multiplier = config->colorizer.parameters.get("hue_speed_multiplier", 1.);
		
		double h = (unsigned(_iterations * hue_speed_multiplier) % 240) / 240. + hue_shift;
		double s = config->colorizer.parameters.get("saturation", 1.);
		double l = config->colorizer.parameters.get("lightness", 0.5);
		return Color::from_hsl(h, s, l);
	}
	
} // namespace Color
