#include "Color/TwoColors.hpp"
#include <cmath>

namespace Color
{
	
	TwoColors::TwoColors(const Configuration* _configuration)
		: Colorizer(_configuration)
	{
		m_color_a = _configuration->colorizer.parameters.get("color_a", Color(0., 0., 0.));
		m_color_b = _configuration->colorizer.parameters.get("color_b", Color(0., 0., 0.));
		m_speed = _configuration->colorizer.parameters.get("speed", 1.);
	}
	
	TwoColors::TwoColors(const Configuration* _configuration, const Color& _color_a, const Color& _color_b, double _speed)
		: TwoColors(_configuration)
	{
		m_color_a = _color_a;
		m_color_b = _color_b;
		m_speed = _speed;
	}
	
	Color TwoColors::get_color(const Complex& _position, unsigned _iterations) const
	{
		return m_color_a + (m_color_b - m_color_a) * ((sin(_iterations * m_speed) + 1) / 2.);
	}
	
} // namespace Color
