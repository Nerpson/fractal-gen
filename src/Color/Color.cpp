#include "Color/Color.hpp"

namespace Color
{
	Color Color::from_hsl(double _h, double _s, double _l)
	{
		double r, g, b;
		if (_s == 0.)
		{
			r = g = b = _l;
		}
		else
		{
			auto hue_to_rgb = [](double _p, double _q, double _t) {
				double p = _p,
					q = _q,
					t = _t;
				
				if (t < 0) t += 1;
				if (t > 1) t -= 1;
				if (t < 1./6.) return p + (q - p) * 6. * t;
				if (t < 1./2.) return q;
				if (t < 2./3.) return p + (q - p) * (2./3. - t) * 6.;
				return p;
			};

			double q = _l < 0.5 ? _l * (1 + _s) : _l + _s - _l * _s;
			double p = 2 * _l - q;
			r = hue_to_rgb(p, q, _h + 1./3.);
			g = hue_to_rgb(p, q, _h);
			b = hue_to_rgb(p, q, _h - 1./3.);
		}
		
		return Color(r, g, b);
	}
	
	Color::Color(double _r, double _g, double _b, double _a)
		: m_r(_r), m_g(_g), m_b(_b), m_a(_a)
	{}

	Color& Color::operator +=(const Color& _right)
	{
		m_r += _right.m_r;
		m_g += _right.m_g;
		m_b += _right.m_b;
		m_a = std::clamp(m_a + _right.m_a, 0., 1.);
		return *this;
	}

	Color& Color::operator /=(double _right)
	{
		m_r /= _right;
		m_g /= _right;
		m_b /= _right;
		return *this;
	}

	bool operator ==(const Color& _left, const Color& _right)
	{
		return (_left.m_r == _right.m_r) && (_left.m_g == _right.m_g) && (_left.m_b == _right.m_b) && (_left.m_a == _right.m_a);
	}

	Color operator +(const Color& _left, const Color& _right)
	{
		return Color(_left.m_r + _right.m_r, _left.m_g + _right.m_g, _left.m_b + _right.m_b, std::clamp(_left.m_a + _right.m_a, 0., 1.));
	}

	Color operator -(const Color& _left, const Color& _right)
	{
		return Color(_left.m_r - _right.m_r, _left.m_g - _right.m_g, _left.m_b - _right.m_b, std::clamp(_left.m_a - _right.m_a, 0., 1.));
	}
	
} // namespace Color
