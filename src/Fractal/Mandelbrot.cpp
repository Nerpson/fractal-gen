#include "Fractal/Mandelbrot.hpp"
#include "Complex.hpp"
#include "Color/Colorizer.hpp"
#include "Color/Ocean.hpp"

namespace Fractal
{
	Color::Color Mandelbrot::render(double _x, double _y, const Color::Colorizer* _colorizer) const
	{
		const Configuration* config = configuration();

		Complex original_z(0., 0.);
		Complex z(original_z);
		Complex c(_y, _x);

		unsigned iterations = 0;
		for (; iterations < config->generator.max_iterations; ++iterations)
		{
			const double magnitude = z.magnitude();
			if (magnitude > 2) break;
			z = z * z + c;
		}

		// Setting the color from the colorizer.
		return _colorizer->get_color(c, iterations);
	}

} // namespace Fractal
