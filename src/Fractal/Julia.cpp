#include "Fractal/Julia.hpp"
#include "Complex.hpp"
#include "Color/Colorizer.hpp"
#include "Color/Spectrum.hpp"

namespace Fractal
{
	Color::Color Julia::render(double _x, double _y, const Color::Colorizer* _colorizer) const
	{
		const Configuration* config = configuration();
		
		Complex original_z(_y, _x);
		Complex z(original_z);
		Complex c(0., 0.);
		c = config->generator.parameters.get("c", c);
		
		unsigned iterations = 0;
		for (; iterations < config->generator.max_iterations; ++iterations)
		{
			const double magnitude = z.magnitude();
			if (magnitude > 2) break;
			z = z * z + c;
		}

		// Setting the color from the colorizer.
		return _colorizer->get_color(original_z, iterations);
	}

} // namespace Fractal
