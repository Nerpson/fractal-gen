#include "Fractal/Generator.hpp"
#include "Fractal/Julia.hpp"
#include "Fractal/Mandelbrot.hpp"

namespace Fractal
{
	std::unique_ptr<Generator> Generator::get(const Configuration* _configuration)
	{
		if (_configuration->generator.name == "Julia")			return std::unique_ptr<Generator>(new Julia(_configuration));
		if (_configuration->generator.name == "Mandelbrot")	return std::unique_ptr<Generator>(new Mandelbrot(_configuration));
		
		const std::string msg = "Unknown generator '" + _configuration->generator.name + "'.";
		throw std::exception(msg.c_str());
	}

} // namespace Fractal

