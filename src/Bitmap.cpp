#include "Bitmap.hpp"
#include "Binary.hpp"

void img_to_bitmap(const Image* _img, Binary& _binary)
{
	_binary.clear();

	// -------- FILE HEADER

	// Magic number.
	_binary.add_bytes("BM", 2);
	
	// BMP file size, to rewrite later.
	_binary.add_integer(0);

	// Creator application identifier.
	_binary.add_byte(0, 2);
	_binary.add_byte(0, 2);
	
	// Offset.
	_binary.add_integer(0);

	// -------- INFO HEADER (DIB)

	// Size of the info header.
	_binary.add_integer(40);

	// Width of the image.
	_binary.add_integer(_img->width());

	// Height of the image.
	_binary.add_integer(_img->height());

	// Number of planes.
	_binary.add_short_integer(1);

	// Bits per pixel.
	_binary.add_short_integer(24); //< 24bits depth = 16M colors.

	// Type of compression.
	_binary.add_integer(0); //< No compression.

	// Compressed image size.
	unsigned byte_size = 3 * _img->width();
	byte_size += 4 - (byte_size % 4);
	byte_size *= _img->height();
	_binary.add_integer(byte_size);

	// Horizontal print resolution in pixel per meter (not ppi).
	_binary.add_integer(2835); //< 2835 = 72dpi | 11811 = 300ppi

	// Vertical print resolution in pixel per meter (not ppi).
	_binary.add_integer(2835); //< 2835 = 72dpi | 11811 = 300ppi

	// Colors in the palette.
	_binary.add_integer(0); //< No palette.

	// Important colors.
	_binary.add_integer(0); //< 0 = all of them.

	// -------- COLOR TABLE

	// None

	// -------- PIXEL DATA

	// Going back writing the data offset.
	_binary.set_integer(10, _binary.size());

	for (int y = int(_img->height()) - 1; y >= 0; --y)
	{
		unsigned line_byte_count = 0;
		
		for (int x = 0; x < int(_img->width()); ++x)
		{
			if (_img->pixel_exists_at(x, y))
			{
				const Pixel& pixel = _img->get_pixel_at(x, y);
				_binary.add_byte(char(255 * pixel.b));
				_binary.add_byte(char(255 * pixel.g));
				_binary.add_byte(char(255 * pixel.r));
			}
			else
			{
				_binary.add_byte(0);
				_binary.add_byte(0);
				_binary.add_byte(0);
			}
			
			line_byte_count += 3;
		}

		while (line_byte_count % 4 != 0) {
			_binary.add_byte(0);
			++line_byte_count;
		}
	}

	// BPM file size.
	_binary.set_integer(2, _binary.size());
}