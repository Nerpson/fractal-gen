#include "Pixel.hpp"

void Pixel::set_color(const Color::Color& _color)
{
	r = _color.r();
	g = _color.g();
	b = _color.b();
}