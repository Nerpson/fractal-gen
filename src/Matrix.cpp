#include "Matrix.hpp"

Matrix::Matrix(unsigned _size_x, unsigned _size_y)
	: m_size_x(_size_x), m_size_y(_size_y)
{}

void Matrix::set(unsigned _x, unsigned _y, double _value)
{
	m_matrix.insert(std::make_pair(std::make_pair(_x, _y), _value));
}

double Matrix::get(unsigned _x, unsigned _y) const
{
	return m_matrix.find(std::make_pair(_x, _y))->second;
}