#include "ImageSharer.hpp"

void ImageSharer::put_color(unsigned _x, unsigned _y, const Color::Color& _color)
{
	while (!m_mutex.try_lock());
	m_image->get_pixel_at(_x, _y).set_color(_color);
	m_mutex.unlock();
}