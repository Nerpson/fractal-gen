#include "FractalGen.hpp"
#include <exception>
#include <chrono>
#include <thread>
#include <algorithm>
#include "Image.hpp"
#include "Binary.hpp"
#include "Bitmap.hpp"
#include "Tile.hpp"
#include "TileQueue.hpp"
#include "ImageSharer.hpp"
#include "RenderThread.hpp"
#include "Effect/Effect.hpp"

FractalGen::FractalGen(int _argc, const char** _argv)
{
	if (_argc != 2)
	{
		std::string msg = "Usage: " + std::string(_argv[0]) + " <json conf file path>\n";
		throw std::exception(msg.c_str());
	}

	m_configuration_file_path = _argv[1];
}

void FractalGen::init()
{
	m_configuration = std::move(Configuration::from_file(m_configuration_file_path));

	printf("Initializing...");
	m_colorizer = std::move(Color::Colorizer::get(m_configuration.get()));
	m_generator = std::move(Fractal::Generator::get(m_configuration.get()));

	m_image.reset(new Image(m_configuration->image.width, m_configuration->image.height));
}

void FractalGen::run()
{
	using namespace std::chrono;

	// Rendering.
	printf("Rendering...\n");
	auto generator_start = steady_clock::now();

	// Compute tiles.
	const unsigned tile_count_x = unsigned(ceil(double(m_image->width()) / m_configuration->generator.tile_size));
	const unsigned tile_count_y = unsigned(ceil(double(m_image->height()) / m_configuration->generator.tile_size));

	TileQueue tile_queue;

	for (unsigned i = 0; i < tile_count_x; ++i)
	{
		for (unsigned j = 0; j < tile_count_y; ++j)
		{
			unsigned from_x = i * m_configuration->generator.tile_size;
			unsigned from_y = j * m_configuration->generator.tile_size;
			unsigned to_x = std::min((i + 1) * m_configuration->generator.tile_size, m_image->width()) - 1;
			unsigned to_y = std::min((j + 1) * m_configuration->generator.tile_size, m_image->height()) - 1;

			tile_queue.queue(new Tile(from_x, from_y, to_x, to_y));
		}
	}

	std::thread** threads = new std::thread*[m_configuration->generator.threads];
	ImageSharer image_sharer(m_image.get());

	for (unsigned i = 0; i < m_configuration->generator.threads; ++i)
	{
		threads[i] = new std::thread(RenderThread(&tile_queue, image_sharer, m_generator.get(), m_colorizer.get()));
	}

	// Waiting for all threads to stop.
	for (unsigned i = 0; i < m_configuration->generator.threads; ++i)
	{
		threads[i]->join();
		delete threads[i];
	}

	delete[] threads;

	auto generator_end = steady_clock::now();
	long long generation_duration = duration_cast<milliseconds>(generator_end - generator_start).count();
	printf("Rendering took %I64dms.\n", generation_duration);

	// Effects.
	if (m_configuration->postprocess.enabled)
	{
		std::unique_ptr<std::vector<std::unique_ptr<Effect::Effect>>> effects = std::move(Effect::Effect::get(m_configuration.get()));
		for (auto it = effects->begin(); it != effects->end(); ++it)
		{
			m_image = std::move((*it)->render(m_image.get()));
		}
	}
	
	// Writing to file.
	printf("Writing to file '%s'...\n", m_configuration->image.filename.c_str());
	auto writer_start = steady_clock::now();
	Binary binary;
	if (m_configuration->image.format == "bmp")
	{
		img_to_bitmap(m_image.get(), binary);
	}
	else
	{
		std::string msg = "Unknown file format: '" + m_configuration->image.format + "'\n";
		throw std::exception(msg.c_str());
	}
	write_to_file(m_configuration->image.filename, binary);
	auto writer_end = steady_clock::now();
	long long writing_duration = duration_cast<milliseconds>(writer_end - writer_start).count();
	printf("Writing took %I64dms.\n", writing_duration);
}
