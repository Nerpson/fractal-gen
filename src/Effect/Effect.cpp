#include "Effect/Effect.hpp"
#include "Effect/Bloom.hpp"
#include "Effect/Matrix.hpp"

namespace Effect
{
	Effect::Effect(const Configuration* _configuration, unsigned _id)
		: m_configuration(_configuration), m_id(_id)
	{}

	std::unique_ptr<std::vector<std::unique_ptr<Effect>>> Effect::get(const Configuration* _configuration)
	{
		std::unique_ptr<std::vector<std::unique_ptr<Effect>>> effects(new std::vector<std::unique_ptr<Effect>>);

		unsigned id = 0;
		for (Configuration::Effect effect : _configuration->postprocess.effects)
		{
			if (effect.name == "Bloom")
			{
				effects->push_back(std::unique_ptr<Effect>(new Bloom(_configuration, id)));
			}
			else if (effect.name == "Matrix")
			{
				effects->push_back(std::unique_ptr<Effect>(new Matrix(_configuration, id)));
			}
			++id;
		}

		return effects;
	}

} // namespace Effect
