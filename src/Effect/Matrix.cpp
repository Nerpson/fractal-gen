#include "Effect/Matrix.hpp"
#include <algorithm>
#include "Color/Color.hpp"
#include "Coord2D.hpp"
#include "Pixel.hpp"
#include "Matrix.hpp"

namespace Effect
{
	std::unique_ptr<Image> Matrix::render(Image* _image) const
	{
		std::unique_ptr<Image> new_image(new Image(*_image));
		const Configuration* config = configuration();

		::Matrix coefficients(0, 0);
		coefficients = config->postprocess.effects[id()].params.get("matrix", coefficients);

		if (coefficients.size_x() % 2 == 1 && coefficients.size_y() % 2 == 1)
		{
			// The matrix has a valid size, X and Y are odd.

			int coefficients_size_x = coefficients.size_x();
			int coefficients_size_y = coefficients.size_y();

			int matrix_center_x = coefficients_size_x / 2;
			int matrix_center_y = coefficients_size_y / 2;
			int matrix_components_count = coefficients_size_x * coefficients_size_y;

			int image_width = _image->width();
			int image_height = _image->height();

			for (int y = 0; y < image_height; ++y)
			{
				for (int x = 0; x < image_width; ++x)
				{
					Color::Color new_color;
					double coefficients_sum = 0.;
					for (int matrix_y = 0; matrix_y < coefficients_size_y; ++matrix_y)
					{
						for (int matrix_x = 0; matrix_x < coefficients_size_x; ++matrix_x)
						{
							unsigned computed_x = unsigned(std::clamp(x + matrix_x - matrix_center_x, 0, image_width - 1));
							unsigned computed_y = unsigned(std::clamp(y + matrix_y - matrix_center_y, 0, image_height - 1));
							double coefficient = coefficients.get(matrix_x, matrix_y);
							new_color += _image->get_pixel_at(computed_x, computed_y).color() * coefficient;
							coefficients_sum += coefficient;
						}
					}
					new_color /= coefficients_sum;
					new_image->get_pixel_at(x, y).set_color(new_color);
				}
			}
		}
		return new_image;
	}

} // namespace Effect
