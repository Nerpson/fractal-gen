#include "Effect/Bloom.hpp"
#include "Pixel.hpp"
#include "Color/Color.hpp"

namespace Effect
{
	std::unique_ptr<Image> Bloom::render(Image* _image) const
	{
		constexpr int falloff_distance = 20;
		std::unique_ptr<Image> new_image(new Image(*_image));
		
		for (unsigned y = 0; y < _image->height(); ++y)
		{
			for (unsigned x = 0; x < _image->width(); ++x)
			{
				Pixel& pixel = _image->get_pixel_at(x, y);
				Color::Color added_color(0., 0., 0.);
				double added_luminosity = 0.;

				for (int delta_y = 0 - falloff_distance / 2; delta_y <= falloff_distance / 2; ++delta_y)
				{
					for (int delta_x = 0 - falloff_distance / 2; delta_x <= falloff_distance / 2; ++delta_x)
					{
						double distance = sqrt(pow(delta_x, 2) + pow(delta_y, 2));
						if (distance > falloff_distance) continue;

						double falloff = distance / falloff_distance;
						added_color = added_color + _image->get_pixel_at(x + delta_x, y + delta_y).color() * falloff;
					}
				}
				
				new_image->get_pixel_at(x, y).set_color(pixel.color() + added_color);
			}
		}
		return new_image;
	}

} // namespace Effect
