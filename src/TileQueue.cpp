#include "TileQueue.hpp"

void TileQueue::queue(Tile* _tile)
{
	while (!m_mutex.try_lock());
	m_queue.push(_tile);
	m_mutex.unlock();
}

Tile* TileQueue::next()
{
	Tile* front_tile = nullptr;
	while (!m_mutex.try_lock());
	if (m_queue.size())
	{
		front_tile = m_queue.front();
		m_queue.pop();
	}
	m_mutex.unlock();
	return front_tile;
}