#include "Binary.hpp"
#include <fstream>

void Binary::add_byte(char _byte)
{
	m_binary.push_back(_byte);
}

void Binary::add_byte(char _byte, unsigned _times)
{
	for (unsigned i = 0; i < _times; ++i)
	{
		add_byte(_byte);
	}
}

void Binary::add_bytes(const char* _bytes, unsigned _size)
{
	for (unsigned i = 0; i < _size; ++i)
	{
		add_byte(_bytes[i]);
	}
}

void Binary::set_byte(unsigned _offset, char _byte)
{
	m_binary[_offset] = _byte;
}

void Binary::add_integer(int _integer)
{
	for (unsigned i = 0; i < 4; ++i)
	{
		add_byte((_integer >> (8 * i)) & 0xFF);
	}
}

void Binary::set_integer(unsigned _offset, int _integer)
{
	for (unsigned i = 0; i < 4; ++i)
	{
		m_binary[_offset + i] = ((_integer >> (8 * i)) & 0xFF);
	}
}

void Binary::add_short_integer(short _short_integer)
{
	for (unsigned i = 0; i < 2; ++i)
	{
		add_byte((_short_integer >> (8 * i)) & 0xFF);
	}
}


void write_to_file(const std::string& _filename, const Binary& _binary)
{
	std::ofstream output_file(_filename, std::ios_base::binary);
	for (auto it = _binary.get_vector().begin(); it != _binary.get_vector().end(); ++it)
	{
		output_file << (*it);
	}
	output_file.close();
}