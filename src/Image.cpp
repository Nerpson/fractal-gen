#include "Image.hpp"
#include <stdexcept>

Image::Image(unsigned _width, unsigned _height)
	: m_width(_width), m_height(_height)
{
	if (_width == 0 || _height == 0) throw std::invalid_argument("Size can't be equal to zero.");
}

Image::Image(const Image& _image)
	: m_width(_image.m_width), m_height(_image.m_height)
{
	for (auto pair : _image.m_registered_pixels)
	{
		m_registered_pixels.insert(std::make_pair(pair.first, new Pixel(*pair.second)));
	}
}

Image::~Image()
{
	for (auto it = m_registered_pixels.begin(); it != m_registered_pixels.end(); ++it)
	{
		delete it->second;
	}
}

bool Image::pixel_exists_at(unsigned _x, unsigned _y) const
{
	auto pair = std::make_pair(_x, _y);
	auto it = m_registered_pixels.find(pair);
	return (it != m_registered_pixels.end());
}

Pixel& Image::get_pixel_at(unsigned _x, unsigned _y)
{
	auto pair = std::make_pair(_x, _y);
	auto it = m_registered_pixels.find(pair);
	if (it != m_registered_pixels.end()) return *(it->second);

	Pixel* new_pixel = new Pixel;
	m_registered_pixels.insert(std::make_pair(pair, new_pixel));

	return *new_pixel;
}

const Pixel& Image::get_pixel_at(unsigned _x, unsigned _y) const
{
	auto pair = std::make_pair(_x, _y);
	return *(m_registered_pixels.find(pair)->second);
}

void Image::remove_pixel_at(unsigned _x, unsigned _y)
{
	m_registered_pixels.erase(m_registered_pixels.find(std::make_pair(_x, _y)));
}